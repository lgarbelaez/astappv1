///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var DashboardController = (function () {
        function DashboardController($scope, $window, getDataCliente) {
            //Controller Body
            this.data = getDataCliente;
            this.numeroPendientes = 0;
            this.opciones = ModelsDefinitions.StatusOptions;
            this.calcularAlerta();
            this.Pendientes();
        }
        DashboardController.prototype.Pendientes = function () {
            var i;
            for (i = 0; i < this.data.length; i++) {
                if (this.data[i].status !== 1 /* Close */) {
                    this.numeroPendientes = this.numeroPendientes + 1;
                }
            }
        };
        DashboardController.prototype.restarFechas = function (fecha1, fecha2) {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        };
        DashboardController.prototype.calcularAlerta = function () {
            var i;
            var resta;
            for (i = 0; i < this.data.length; i++) {
                resta = Math.abs(this.restarFechas(this.data[i].assignationDate.getTime(), new Date().getTime()));
                if (resta > 24 && this.data[i].status === 0 /* Open */) {
                    this.data[i].color = 'list-red';
                }
                if (resta <= 24 && this.data[i].status === 0 /* Open */) {
                    this.data[i].color = 'list-yellow';
                }
                if (resta > 24 && this.data[i].status === 2 /* Pending */) {
                    this.data[i].color = 'list-purple';
                }
                if (resta <= 24 && this.data[i].status === 2 /* Pending */) {
                    this.data[i].color = 'list-orange';
                }
            }
        };
        DashboardController.$inject = ['$window', '$q', 'getDataCliente'];
        return DashboardController;
    })();
    angular.module('ASTAppControllers.controllers').controller('ASTAppControllers.controllers.DashboardController', DashboardController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/dashboard',
//                        templateUrl: 'templates/dashboard-template.html',
//                        controller: 'dashboard.controllers.DashboardController as dashboard'
//                    });
//For use inside template:
//    {{dashboard.data}}
//Check dependencies inside app.ts
//    angular.module('dashboard.controllers', []);
//    angular.module('app', ['ionic', 'dashboard.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashboardcontroller.js"></script> 
//# sourceMappingURL=dashboardController.js.map