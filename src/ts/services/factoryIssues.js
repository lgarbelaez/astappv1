///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppFactory;
(function (ASTAppFactory) {
    IssueFactory.$inject = ['$http', '$window'];
    function IssueFactory($http, $window) {
        var data;
        var ubicacion1;
        data = [
            'value 1',
            'value 2'
        ];
        return {
            getData: function () {
                return data;
            },
            getDataIndex: function (index) {
                return data[index];
            },
            putData: function (index, value) {
                data[index] = value;
            }
        };
    }
    angular.module('ASTApp.factory').factory('ASTApp.factory.IssueFactory', IssueFactory);
})(ASTAppFactory || (ASTAppFactory = {}));
//# sourceMappingURL=factoryIssues.js.map