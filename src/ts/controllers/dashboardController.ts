///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppControllers {
    'use strict';

    class DashboardController {

        data: Array<ModelsDefinitions.Issue>;
        numeroPendientes: number;
        opciones: any;

        static $inject = [ '$window', '$q', 'getDataCliente'];
        constructor($scope, $window, getDataCliente) {
            //Controller Body
            this.data = getDataCliente;
            this.numeroPendientes = 0;
            this.opciones = ModelsDefinitions.StatusOptions;
            this.calcularAlerta();
            this.Pendientes();


     }
        Pendientes(): void {
            var i : number;
            for (i = 0; i < this.data.length; i++) {
                if (this.data[i].status !== ModelsDefinitions.StatusOptions.Close) {
                    this.numeroPendientes = this.numeroPendientes + 1;
                }
            }
        }

        restarFechas(fecha1: number, fecha2: number) : number {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        }

        calcularAlerta(): void {

            var i : number;
            var resta : number;

            for (i = 0; i < this.data.length; i++) {

                resta = Math.abs(this.restarFechas(this.data[i].assignationDate.getTime(), new Date().getTime()));
                if (resta > 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.data[i].color = 'list-red';
                }
                if (resta <= 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.data[i].color = 'list-yellow';
                }
                if (resta > 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.data[i].color = 'list-purple';
                }
                if (resta <= 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.data[i].color = 'list-orange';
                }
            }
        }

    }

    angular.module('ASTAppControllers.controllers')
        .controller('ASTAppControllers.controllers.DashboardController', DashboardController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/dashboard',
//                        templateUrl: 'templates/dashboard-template.html',
//                        controller: 'dashboard.controllers.DashboardController as dashboard'
//                    });

//For use inside template:
//    {{dashboard.data}}

//Check dependencies inside app.ts
//    angular.module('dashboard.controllers', []);
//    angular.module('app', ['ionic', 'dashboard.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashboardcontroller.js"></script>