///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var IssuesController = (function () {
        function IssuesController($window, getDataCliente) {
            this.data = getDataCliente;
        }
        IssuesController.$inject = ['$window', 'getDataCliente'];
        return IssuesController;
    })();
    angular.module('ASTAppControllers.controllers').controller('ASTAppControllers.controllers.IssuesController', IssuesController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issues',
//                        templateUrl: 'templates/issues-template.html',
//                        controller: 'issues.controllers.IssuesController as issues'
//                    });
//For use inside template:
//    {{issues.data}}
//Check dependencies inside app.ts
//    angular.module('issues.controllers', []);
//    angular.module('app', ['ionic', 'issues.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issuescontroller.js"></script>
//# sourceMappingURL=issuesController.js.map