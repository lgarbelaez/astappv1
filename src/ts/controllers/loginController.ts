///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppControllers {
    'use strict';

    class LoginController {

        data: string[];

        static $inject = ['$window'];
        constructor($window) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];

        }
    }

    angular.module('ASTAppControllers.controllers')
        .controller('ASTAppControllers.controllers.LoginController', LoginController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/login',
//                        templateUrl: 'templates/login-template.html',
//                        controller: 'loginControllers.controllers.LoginController as login'
//                    });

//For use inside template:
//    {{login.data}}

//Check dependencies inside app.ts
//    angular.module('loginControllers.controllers', []);
//    angular.module('app', ['ionic', 'loginControllers.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/logincontroller.js"></script>