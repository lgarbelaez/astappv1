///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />
///<reference path="entities/entities.ts"/>

((): void => {

    angular.module('app', ['ionic', 'Dependencies', 'ngCordova', 'uiGmapgoogle-maps'])

        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    window.StatusBar.styleDefault();
                }
            });
        })
        .config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider',
        ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) => {

            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyATPx0Z-w2U9Ac1WDgIGW6awopLOh2JoII',
                v: '3.17',
                libraries: 'weather,geometry,visualization'});

            $urlRouterProvider.otherwise('/login');
            var listaIncidencias : Array<ModelsDefinitions.Issue>;
            $stateProvider
                  .state('login', {
                    url: '/login',
                     views: {
                         login: {
                          templateUrl: 'templates/login-template.html',
                          controller : 'ASTAppControllers.controllers.LoginController as login'
                          }
                     }
                  })
                   .state('dashboard', {
                      url: '/dashboard',

                    resolve:{
                            getDataCliente : function(){

                                var cliente = {name:'Luis Guillermo', address:'Car 16 #pra', location: {latitude:'', longitude:''}};
                                var usuario = {userName: '', password:'', location:{latitude:'', longitude:''}};
                                var estado: ModelsDefinitions.StatusOptions;
                                var estado1: ModelsDefinitions.StatusOptions;
                                var estado2: ModelsDefinitions.StatusOptions;
                                estado = ModelsDefinitions.StatusOptions.Open;
                                estado1 = ModelsDefinitions.StatusOptions.Close;
                                estado2 = ModelsDefinitions.StatusOptions.Pending;

                                var fecha: Date;
                                var fecha2: Date;
                                var fecha3: Date;
                                fecha = new Date('02/03/2015');
                                fecha2 = new Date('11/09/2014');
                                fecha3 = new Date('01/01/2015');


                                var issue:ModelsDefinitions.Issue;

                                issue = {
                                    id:1,
                                    status:estado,
                                    client:cliente,
                                    assignee: usuario,
                                    assignationDate:fecha,
                                    creationDate:fecha,
                                    dateLastModification:fecha,
                                    color: '',
                                    info:'Issue #1',
                                    comments:'Comentario 1111',
                                    urlFoto:'img/imagen.jpg'};

                                var issue2:ModelsDefinitions.Issue;

                                issue2 = {
                                    id:2,
                                    status:estado2,
                                    client:cliente,
                                    assignee: usuario,
                                    assignationDate:fecha2,
                                    creationDate:fecha2,
                                    dateLastModification:fecha2,
                                    info:'Issue #2',
                                    comments:'Comentario PENDIENTE',
                                    color: ''};

                                var issue3:ModelsDefinitions.Issue;
                                issue3 = {
                                    id:3,
                                    status:estado,
                                    client:cliente,
                                    assignee: usuario,
                                    assignationDate:fecha3,
                                    creationDate:fecha3,
                                    dateLastModification:fecha3,
                                    color: '',
                                    info:'Issue #3',
                                    comments:'SE ENCUENTRA ABIERTA FALTA'};

                                var issue4:ModelsDefinitions.Issue;
                                issue4 = {
                                    id:4,
                                    status:estado1,
                                    client:cliente,
                                    assignee: usuario,
                                    assignationDate:fecha3,
                                    creationDate:fecha3,
                                    dateLastModification:fecha3,
                                    color: '',
                                    info:'Issue #4',
                                    comments:'Comentario buen cliente'};

                                listaIncidencias = [issue, issue2, issue3, issue4];
                                return  listaIncidencias;
                            }
                    },
                       views: {
                           dashboard :{
                               templateUrl: 'templates/dashboard-template.html',
                               controller: 'ASTAppControllers.controllers.DashboardController as Mydashboards'

                           }
                       }
                  })
                    .state('issues', {
                      url: '/issues',

                        resolve:{
                            getDataCliente : function(){

                            var cliente = {name:'Luis Guillermo', address:'Car 16 #pra', location: {latitude:'', longitude:''}};
                            var usuario = {userName: '', password:'', location:{latitude:'', longitude:''}};
                            var estado: ModelsDefinitions.StatusOptions;
                            var estado1: ModelsDefinitions.StatusOptions;
                            var estado2: ModelsDefinitions.StatusOptions;
                            estado = ModelsDefinitions.StatusOptions.Open;
                            estado1 = ModelsDefinitions.StatusOptions.Close;
                            estado2 = ModelsDefinitions.StatusOptions.Pending;

                            var fecha: Date;
                            var fecha2: Date;
                            var fecha3: Date;
                            fecha = new Date('02/03/2015');
                            fecha2 = new Date('11/09/2014');
                            fecha3 = new Date('01/01/2015');

                            var issue:ModelsDefinitions.Issue;

                            issue = {
                                id:1,
                                status:estado,
                                client:cliente,
                                assignee: usuario,
                                assignationDate:fecha,
                                creationDate:fecha,
                                dateLastModification:fecha,
                                info:'Issue #1',
                                comments:'Comentario 1111',
                                urlFoto:'img/imagen.jpg',
                                color: ''};

                            var issue2:ModelsDefinitions.Issue;

                            issue2 = {
                                id:2,
                                status:estado1,
                                client:cliente,
                                assignee: usuario,
                                assignationDate:fecha2,
                                creationDate:fecha2,
                                dateLastModification:fecha2,
                                info:'Issue #2',
                                comments:'Comentario 222',
                                color: ''};

                            var issue3:ModelsDefinitions.Issue;
                            issue3 = {
                                id:3,
                                status:estado2,
                                client:cliente,
                                assignee: usuario,
                                assignationDate:fecha3,
                                creationDate:fecha3,
                                dateLastModification:fecha3,
                                info:'Issue #3',
                                comments:'Comentario 333',
                                color:''};

                            listaIncidencias = [issue, issue2, issue3];
                            return  listaIncidencias;
                        }
                    },
                            templateUrl: 'templates/issues-template.html',
                             controller: 'ASTAppControllers.controllers.IssuesController as Myissues'
                   })
                   .state('cliente', {
                    url: '/cliente',
                        views: {
                            cliente: {
                            templateUrl: 'templates/Client-template.html',
                            controller: 'ASTAppControllers.controllers.ClienteController as clienteController'
                        }
                    },
                     resolve: {
                        getClientesLocal: function () {
                            var ubicacion: ModelsDefinitions.Location;
                            ubicacion = {
                                latitude: '4.805767',
                                longitude: '-75.690643'
                            };
                            var cliente: ModelsDefinitions.Client;
                            cliente = {
                                name: 'Julian Carmona',
                                address: 'Cra 7 # 21-20',
                                urlFoto:'img/imagen.jpg',
                                location: ubicacion
                            };
                            return cliente;
                        }

                    }})
                .state('mapa', {
                    url: '/mapa',
                        views: {
                            mapa: {
                                templateUrl: 'templates/mapa-template.html',
                                controller: 'ASTAppControllers.controllers.MapaController as mapaController'
                            }

                        },
                        resolve: {
                            getClientesLocal: function () {
                                var ubicacion: ModelsDefinitions.Location;
                                ubicacion = {
                                    latitude: '4.805767',
                                    longitude: '-75.690643'
                                };
                                var cliente: ModelsDefinitions.Client;
                                cliente = {
                                    name: 'Julian Carmona',
                                    address: 'Cra 7 # 21-20',
                                    location: ubicacion
                                };
                                return cliente;
                            }

                        }
                    })


            .state('detail', {
                url: '/issues/:id',
                    resolve:{
                        getIssue:['$stateParams', function($stateParams){
                            var index: number;
                            for (index = 0; index < listaIncidencias.length; index++) {
                                if (listaIncidencias[index].id === parseInt($stateParams.id, 10)) {
                                    return listaIncidencias[index];
                                }
                            }
                        }]
                    },
                templateUrl: 'templates/detail-template.html',
                controller : 'ASTAppControllers.controllers.DetailController as detail'
            });


            //
            //.state('app.issue-detail', {
            //    url: "/issues/:id",
            //    views: {
            //        'menuContent': {
            //            templateUrl: "templates/issue-detail.html",
            //            controller: 'PlaylistsCtrl'
            //        }
            //    }
            //})






        }]);
})();