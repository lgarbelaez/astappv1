///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>

module ASTAppControllers {
    'use strict';

    class IssuesController {

        data: string[];


        static $inject = ['$window', 'getDataCliente'];
        constructor($window, getDataCliente) {

            this.data = getDataCliente;

        }
    }

    angular.module('ASTAppControllers.controllers')
        .controller('ASTAppControllers.controllers.IssuesController', IssuesController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issues',
//                        templateUrl: 'templates/issues-template.html',
//                        controller: 'issues.controllers.IssuesController as issues'
//                    });

//For use inside template:
//    {{issues.data}}

//Check dependencies inside app.ts
//    angular.module('issues.controllers', []);
//    angular.module('app', ['ionic', 'issues.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issuescontroller.js"></script>
