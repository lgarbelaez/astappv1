/**
 * Created by lgarbelaez on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var DetailController = (function () {
        function DetailController(getIssue) {
            //Controller Body
            this.datosDetalle = getIssue;
            this.data = ['valor 1', 'valor 2'];
        }
        DetailController.$inject = ['getIssue'];
        return DetailController;
    })();
    angular.module('ASTAppControllers.controllers').controller('ASTAppControllers.controllers.DetailController', DetailController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/detail',
//                        templateUrl: 'templates/detail-template.html',
//                        controller: 'ASTApp.controllers.DetailController as detail'
//                    });
//For use inside template:
//    {{detail.data}}
//Check dependencies inside app.ts
//    angular.module('ASTApp.controllers', []);
//    angular.module('app', ['ionic', 'ASTApp.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/detailcontroller.js"></script> 
//# sourceMappingURL=issue-detailController.js.map