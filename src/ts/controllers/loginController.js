///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var LoginController = (function () {
        function LoginController($window) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
        }
        LoginController.$inject = ['$window'];
        return LoginController;
    })();
    angular.module('ASTAppControllers.controllers').controller('ASTAppControllers.controllers.LoginController', LoginController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/login',
//                        templateUrl: 'templates/login-template.html',
//                        controller: 'loginControllers.controllers.LoginController as login'
//                    });
//For use inside template:
//    {{login.data}}
//Check dependencies inside app.ts
//    angular.module('loginControllers.controllers', []);
//    angular.module('app', ['ionic', 'loginControllers.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/logincontroller.js"></script> 
//# sourceMappingURL=loginController.js.map