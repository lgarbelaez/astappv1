///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppFactory {

    export interface IIssueFactory {
        getData():string[];
        getDataIndex(index:number):string;
        putData: (index:number, value:string) => void;
    }
    IssueFactory.$inject = ['$http', '$window'];
    function IssueFactory ($http, $window) : IIssueFactory {

        var data: string[];
        var ubicacion1: ModelsDefinitions.Location;
        data = [
            'value 1',
            'value 2'
        ];

        return {
            getData: () => { return data; },
            getDataIndex: function (index: number) {
                return data[index];
            },
            putData: function (index:number, value:string) {
                data[index] = value;
            }

        };
    }


    angular.module('ASTApp.factory')
        .factory('ASTApp.factory.IssueFactory', IssueFactory);
}

